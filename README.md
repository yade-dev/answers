# Questions and Answers

Yade user Question and Answers are now hosted as [issues](https://gitlab.com/yade-dev/answers/-/issues) in the [yade-dev/answers](https://gitlab.com/yade-dev/answers) repository. Old questions used to be hosted on [answers.launchpad.net](https://answers.launchpad.net/yade/+questions), but they were all migrated to the present Gitlab. This means that if you search in [yade-dev/answers](https://gitlab.com/yade-dev/answers/-/issues), you should find archived questions as well as new questions.

## Formatting questions

Please do your best to provide as much information as possible, including all information requested in our [How to Ask](#how-to-ask) guide. If you post code, please follow standard markdown practices. This [Gitlab markdown guide](https://docs.gitlab.com/ee/user/markdown.html) covers all necessary practices. Some of the most important pieces are:

### Provide Yade version and Linux version

Yade runs many versions on many linux systems. If you do not provide these details in your question, we will struggle to help you.
Simplest way to achieve this is to directly paste the output of command `printAllVersions()` ran in yade console.

### Use code blocks

If you plan to post code, please enclose your code using three backticks as [highlighted here](https://docs.gitlab.com/ee/user/markdown.html#code-spans-and-blocks). This means your code will look nice and pretty:

```python
for b in O.bodies:
    print(f"Body {b.id} present")
```

### Make your material concise and reproducible

If you have an error, please ensure you post all materials *on gitlab* to help reproduce the error. Additionally, these materials should be a Minimum Working Example (MWE). This means that this script should be the smallest script possible to reproduce the error. If it includes a definition of hundreds of unused variables/functions, we will have trouble helping you.


### Attach images/files

Please attach images to help describe your problem. This should be as easy as taking a screenshot and then dragging it into your Gitlab form. Additionally, feel free to attach python scripts by also dragging and dropping them into the gitlab form.

### Refer to old related questions

Yade is over 10 years old, this means that it is highly likely we answered a similar question before. Please link to these related questions to help give us context.


### Include the **full** error

Errors provide most of the information necessary for finding and fixing bugs. If you do not provide the error, or you only provide part of the error, we cannot help you.


### Be polite

Yade is open-source volunteer based project. Please remain respectful of the people helping you and try to treat them the way you would like to be treated. If they are asking you for more information - it is because they need more information.


## How to ask

We have kept this around to help people understand how to properly construct questions. If you were referred to this section, it means you will benefit from reading it:

How to ask questions as a user of Yade (or any other computer code)


### Search before asking!

Before asking any question, you may consider searching through the mailing list. Probably someone faced a similar problem before and got a solution. The most convenient way to search among previous questions is to simply use the [Gitlab issues search](https://gitlab.com/yade-dev/answers/-/issues/) or by typing some keywords together with "site:https://gitlab.com/project/yade-dev/answers" in your favourite search engine. 

### Build your question! 

- Please don't send questions directly by emails to yade-users. Instead, use the issues provided in [Gitlab](https://gitlab.com/yade-dev/answers/-/issues/) which helps sorting questions and answers. Once, a question is asked there, you will still receive answers in your mailbox, and you can "reply" to them with your email client. Do not append your question after an older question (whoever asked the older one).
- Try to describe your problem in a concise and accurate way. Generally, this is achieved by isolating the problem to a single line in the script (e.g. "when I turn *this* on, it works, when I turn *that* off, it throws the following error"). If you encounter an error, share it. Some context can be useful sometimes (I'm trying to achieve this or that, I'm beginner/advanced/expert in Python, etc.) but in most cases it does not help a lot, it should be short.
- Together with problem statement, send also a script which will let others reproduce your problem. Do your best to send it as a MWE (Minimal Working Example™). The MWE should actually be the smallest script possible showing the same problem you faced probably in a much longer script, initially. It must be:
    - minimal = short. It is difficult to find mistakes in long pieces of code and it is usually much more annoying. Any post-processing command should for instance be banned.
    - working = before sending, test it to be sure it is working (it has no syntax errors etc.).
- Together with the above information, send also the version of Yade you are using. If your question is linked to compilation issues, tell which operating system you use and show the logs of cmake and/or make.
- Do not merge different questions in one single post, even if - in your mind - they are connected to one single project. If your question received satisfying answer(s), mark it answered immediately. Do not use the same thread to jump to another question, open a new question instead.
- Train your empathy on this example, a fictious but archetypal question which typically appears without further info. What is the probability for programming gurus to understand the problem in your opinion?
    "Hello, I was trying to modelize a problem but python was returning an error. So, I changed some engine but now yade crashes. Can you please help?"

### Avoid external links!

- External links mean the potential responder will need to click multiple times just to read the question, while he/she is possibly reading it offline (via emails) and on an arbitrary device (e.g. cell phone). This potential responder might then simply move to next question (some of them even systematically do so).
- The persistence of external links over time is not guaranteed, it will make the archive incomplete.
- External links are not properly crawled into by search engines
- Although removing external links may need a bit more work before sending the question, this extra time spent by the person asking means less time lost by the person answering. This is just fair. Moreover, by doing so you may well understand the problem by yourself in many cases. 

### Add concise scripts!

- Showing a script should be possible in Gitlab (or attaching the file directly), since the scripts are supposed to be short (see previous section on MWE).
- If the input of one script is the output of another script, then both scripts should be merged into one. That is: avoid sending multiple scripts just like you avoid sending input files to a script. If needed, the method to save and reload in one single script is illustrated [here](https://gitlab.com/yade-dev/trunk/-/blob/master/examples/simple-scene/save-then-reload.py).
- Error logs (be it compilation or runtime error) can be long, sometimes, yet the relevant lines are not many. If the log is very long please identify the error which comes first and paste only that one (together with user's input command).
- Input files with particle coordinates or input mesh are not needed, in most cases. They can be replaced by writing coordinates directly in the script or using makeCloud or similar functions. Most problems are not due to a specific number of particles or the specific shape of an imported geometry. Instead, they can be reproduced with just a few facets and a few spheres (or clumps or whatever is in a specific situation).

